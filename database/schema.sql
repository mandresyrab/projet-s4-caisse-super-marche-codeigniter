create table product(
    code varchar(3) primary key,
    chemin text not null,
    designation varchar(20) not null,
    pu real check(pu>0)
);

create table panier(
    idpanier varchar(5) primary key,
    code varchar(3),
    qty int check(qty>0),
    foreign key (code) references product(code)
);

create table remise(
    idremise varchar(3) PRIMARY key,
    [tablename] varchar(50),
    [condition]  varchar(50),
    [date] date
);