# Projet S4 - Code Igniter SI -IHM TO DO LIST

<h1>To Do List</h1>

<h2>Créer la table produit </h2>
    <ol>
        <li>Code</li>
        <li>Designation</li>
        <li>Prix unitaire</li>
    </ol>

<h2>Créer la table panier</h2>
    <ol>
        <li>Code</li>
        <li>Date</li>
        <li>Qty</li>
    </ol>

<h2>Créer la table remise</h2>
    <ol>
        <li>Code</li>
        <li>Condition</li>
        <li>Taux</li>
    </ol>

<h2>Créer la vue caisse avec MySQL</h2>
<h2>Créer la classe de connection à la base</h2>
<h2>Créer la classe Panier_Model extends CI_Model</h2>
    <ul>
        <li>getCode</li>
        <li>getDesignation</li>
        <li>getPU</li>
        <li>getQty</li>
        <li>getRemise</li>
        <li>getMontant</li>
    </ul>

<h2>Créer la classe Caisse_Model extends CI_Model</h2>
    <ul>
        <li>listePanier[]</li>
        <li>getTotal</li>
        <li>delete(id)</li>
        <li>commit()</li>
    </ul>

<h2>Créer la classe Caisse_Controller extennds CI_Controller</h2>
    <ul>
        <li>index  (Redirection vers la page de saisie des produits)</li>
        <li>insert (Insertion de nouveau produits)</li>
        <li>commit (Validation de l'achat de la liste  de produit)</li>
    </ul>